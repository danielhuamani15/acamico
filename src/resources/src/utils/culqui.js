import vuex from '@/vuex'

window.culqi = function token(culquiToken) {
  if (culquiToken.token) {
    var data = culquiToken.token
    vuex.dispatch('setCulqui', {
      dataToken: data,
      errorToken: {},
      isErrorToken: false
    })
  } else {
    var data = culquiToken.error
    vuex.dispatch('setCulqui', {
      dataToken: {},
      errorToken: data,
      isErrorToken: true
    })
  }
  console.log(culquiToken.token, 'token-----', culquiToken.error)
}

export default function CulqiUtil (Culqui) {
  return new Promise(
    function (resolve, reject) {
      Culqui.createToken()
      let checkToken = setInterval(function() {
        if (vuex.getters.getIsErrorCulqi) {
          reject('error')
        } else {
          clearInterval(checkToken)
          resolve(vuex.getters.getDataTokenCulqi)
        }
      }, 4000)
    }
  )
}