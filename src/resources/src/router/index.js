import Vue from 'vue'
import Router from 'vue-router'
import categoryList from '@/pages/categoryList'
import influencer from '@/pages/influencer'
import influencerNew from '@/pages/influencerNew'
import productDetail from '@/pages/productDetail'
import productFavorites from '@/pages/productFavorites'
import checkout from '@/pages/checkout'
import checkoutPaso1 from '@/pages/checkoutPaso1'
import checkoutPaso2 from '@/pages/checkoutPaso2'
import checkoutPaso3 from '@/pages/checkoutPaso3'
import cart from '@/pages/cart'
// import cartView from '@/common/cartView'
Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/categoria/:slug_parent_category/:slug_category/',
      name: 'category_list',
      component: categoryList
    },
    {
      path: '/mis-favoritos/',
      name: 'product_favorites',
      component: productFavorites
    },
    {
      path: '/producto/:slug/',
      name: 'product_detail',
      component: productDetail
    },

    {
      path: '/influenciador-new/:slug/',
      name: 'influencerNew',
      component: influencerNew
    },
    {
      path: '/marcas-disenadores/:slug/',
      name: 'influencer',
      component: influencer
    },
    {
      path: '/carro-compras/',
      name: 'cart',
      component: cart
    },
    {
      path: '/proceso-compra/',
      name: 'checkout',
      component: checkout,
      children: [
        {
          path: 'paso-1//',
          name: 'checkout_paso_1',
          component: checkoutPaso1
        },
        {
          path: 'paso-2//',
          name: 'checkout_paso_2',
          component: checkoutPaso2
        },
        {
          path: 'paso-3//',
          name: 'checkout_paso_3',
          component: checkoutPaso3
        }
      ]
    }
  ]
})
