import Vue from 'vue'

const moduleCulqui = {
  state: {
    dataToken: {

    },
    error: {

    },
    isError: false
  },
  getters: {
    getDataTokenCulqi: state => {
      return state.dataToken
    },
    getErrorCulqi: state => {
      return state.error
    },
    getIsErrorCulqi: state => {
      return state.isError
    }
  },
  mutations: {
    setDataTokenCulqi (state, dataToken) {
      state.dataToken = dataToken
    },
    setErrorCulqi (state, error) {
      state.error = error
    },
    setIsErrorCulqi (state, isError) {
      state.isError = isError
    }
  },
  actions: {
    setCulqui ({commit}, dataCulqui) {
      commit('setDataTokenCulqi', dataCulqui.dataToken)
      commit('setErrorCulqi', dataCulqui.errorToken)
      commit('setIsErrorCulqi', dataCulqui.isErrorToken)
    }
  }
}

export default moduleCulqui