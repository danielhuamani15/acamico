import Vue from 'vue'
import Vuex from 'vuex'
import moduleCartView from './cartView'
import moduleCulqui from './culqui'
Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    moduleCartView: moduleCartView,
    moduleCulqui: moduleCulqui
  }
})

export default store
