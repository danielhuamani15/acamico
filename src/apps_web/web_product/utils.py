from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response


class ProductPagination(PageNumberPagination):
    page_size = 12
    page_size_query_param = 'page_size'
    max_page_size = 1000

    def get_paginated_response(self, data):
        return Response({
            'links': {
                'next': self.get_next_link(),
                'previous': self.get_previous_link()
            },
            'count': self.page.paginator.count,
            'total_pages': self.page.paginator.num_pages,
            'results': data,
            'current_page': self.request.query_params.get('page', 1)
        })

def abc():
    abecedario = {
        'A-B': ['A', 'B', 'a', 'b'],
        'C-D': ['C', 'D', 'c', 'd'],
        'E-F': ['E', 'F', 'e', 'f'],
        'G-H': ['G', 'H', 'g', 'h'],
        'I-J': ['I', 'J', 'i', 'j'],
        'K-L': ['K', 'L', 'k', 'l'],
        'M-N': ['M', 'N', 'k', 'm'],
        'O-P': ['O', 'P', 'o', 'p'],
        'Q-R': ['Q', 'R', 'q', 'r'],
        'S-T': ['S', 'T', 's', 't'],
        'U-V': ['U', 'V', 'u', 'v'],
        'X-Y': ['X', 'Y', 'x', 'y']
    }
    return abc