from django.conf.urls import url
from .views import (checkout_paso_1, checkout_paso_2, checkout_thanks,
    get_price_shipping, get_coupon_discount, checkout_new)
from .api_views import (InfluencerShippingProductAPI, CustomerShippingAddressAPI, CustomerAPI,
    CustomerShippingAddressUpdateAPI)

urlpatterns = [
    url(r"^checkout/paso-1/$", checkout_paso_1, name="checkout_paso_1"),
    url(r"^checkout/paso-2/$", checkout_paso_2, name="checkout_paso_2"),
    url(r"^proceso-compra/paso-1/$", checkout_new, name="checkout_new"),
    url(r"^proceso-compra/paso-2/$", checkout_new, name="checkout_new_2"),
    url(r"^proceso-compra/paso-3/$", checkout_new, name="checkout_new_3"),
    url(r"^proceso-compra/gracias/$", checkout_thanks, name="checkout_thanks"),
    url(r"^ubigeo-price/$", get_price_shipping, name="get_price_shipping"),
    url(r"^validate-coupon/$", get_coupon_discount, name="get_coupon_discount"),
    url(r"^customer-shipping-address/$",
        CustomerShippingAddressAPI.as_view(),
        name="customer_shipping_address"),
    url(r"^customer-shipping-address/(?P<pk>\d+)/$",
        CustomerShippingAddressUpdateAPI.as_view(),
        name="customer_shipping_address_update"),
    url(r"^api/influencer-shipping-product/$",
        InfluencerShippingProductAPI.as_view(),
        name="influencer_shipping_product"),
    url(r"^api/order-customer/$",
        CustomerAPI.as_view())
]

