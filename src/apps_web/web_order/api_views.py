from rest_framework.views import APIView
from rest_framework.response import Response
from apps_base.shipping.models import ShippingCost
from apps_base.customers.models import CustomerShippingAddress
from apps_base.cart.models import Cart, CartItem
from apps_base.influencer.models import Influencer
from rest_framework.generics import UpdateAPIView, ListAPIView, RetrieveUpdateAPIView, ListCreateAPIView
from rest_framework.viewsets import ModelViewSet
from .serializers import InfluencerOrderSerializer, CustomerShippingAddressSerializer, CustomerSerializer, UserSerializer


class CustomerShippingAddressAPI(ListCreateAPIView):
    # queryset = Cart.objects.none()
    serializer_class = CustomerShippingAddressSerializer

    def get_queryset(self):
        user = self.request.user
        customer = user.user_customer
        return CustomerShippingAddress.objects.filter(customer=customer)

    def perform_create(self, serializer):
        user = self.request.user
        customer = user.user_customer
        serializer_customer_shipping = serializer.save(customer=customer)


class CustomerShippingAddressUpdateAPI(RetrieveUpdateAPIView):
    # queryset = Cart.objects.none()
    serializer_class = CustomerShippingAddressSerializer

    def get_queryset(self):
        user = self.request.user
        customer = user.user_customer
        return CustomerShippingAddress.objects.filter(customer=customer)

    # def get(self, request, format=None):
    #     address = request.query_params.get('address', None)
    #     if address:
    #         try:
    #             customer_shipping_addres = CustomerShippingAddress.objects.get(
    #                 id=int(address))
    #             serializer = CustomerShippingAddressSerializer(customer_shipping_addres)
    #             return Response(serializer.data, status=200)
    #         except Exception as e:
    #             return Response({}, status=403)
    #     return Response({}, status=403)


class InfluencerShippingProductAPI(APIView):

    def get(self, request, format=None):
        user = request.user
        customer = user.user_customer
        ubigeo = request.query_params.get('ubigeo')
        code = request.COOKIES.get('cart', None)
        cart_items = CartItem.objects.filter(
            cart__code=code)
        influencer_ids = cart_items.values_list('product__product_class__influencer_id', flat=True)
        cart = Cart.objects.get(code=code)
        influencers = Influencer.objects.filter(id__in=influencer_ids)
        serializers = InfluencerOrderSerializer(
            influencers,
            context={'request': request, 'cart_items': cart_items, 'ubigeo': ubigeo}, many=True)
        return Response(serializers.data, status=200)


class CustomerAPI(APIView):
    serializer_class = CustomerSerializer

    def get_object(self):
        return self.request.user.user_customer

    def get(self, request, format=None):
        user = request.user
        customer = user.user_customer
        serializer_user = UserSerializer(user)
        serializers = CustomerSerializer(customer, context={'request': request})
        data = {
            'customer': serializers.data,
            'user': serializer_user.data
        }
        return Response(data, status=200)

    def put(self, request, format=None):
        user = request.user
        customer = user.user_customer
        data = request.data
        client_user = data.get('user')
        client_customer = data.get('customer')
        # user_data = data.pop('user')
        # print(customer, 'customer', data, user_data)
        serializer = CustomerSerializer(customer, data=client_customer)
        serializer_user = UserSerializer(user, data=client_user)
        if serializer.is_valid() and serializer_user.is_valid():
            serializer.save()
            serializer_user.save()
            data = {
                'customer': serializer.data,
                'user': serializer_user.data
            }
            return Response(data)
        else:
            errors = {
                'customer': serializer.errors,
                'user': serializer_user.errors
            }
            return Response(errors, status=403)