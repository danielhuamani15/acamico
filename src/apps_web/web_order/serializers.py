from rest_framework import serializers
from drf_queryfields import QueryFieldsMixin
from apps_base.customers.models import CustomerShippingAddress, Customer
from apps_base.custom_auth.models import User
from rest_framework.validators import UniqueValidator
from apps_base.influencer.models import Influencer
from apps_web.web_cart.serializers import CartItemSerializer
from apps_base.shipping.models import ShippingMethod, ShippingCost
from datetime import datetime, date, timedelta


class CustomerShippingAddressSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    departamento = serializers.SerializerMethodField()
    provincia = serializers.SerializerMethodField()
    type_address_name = serializers.SerializerMethodField()

    class Meta:
        model = CustomerShippingAddress
        fields = [
            'first_name', 'last_name', 'type_document', 'document',
            'phone', 'address', 'reference', 'ubigeo', 'departamento', 'provincia',
            'id', 'type_address', 'type_address_name'
        ]

    def get_type_address_name(self, obj):
        return obj.get_type_address_display()

    def get_departamento(self, obj):
        return obj.ubigeo.cod_dep_inei

    def get_provincia(self, obj):
        return obj.ubigeo.cod_prov_inei


class UserSerializer(serializers.ModelSerializer):
    # email = serializers.EmailField(
    #         required=True,
    #         validators=[UniqueValidator(queryset=User.objects.all())]
    #         )

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name']


class CustomerSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = Customer
        fields = ['document', 'type_document',
            'gender', 'phone']


class ShippingMethodSerializer(QueryFieldsMixin, serializers.ModelSerializer):

    class Meta:
        model = ShippingMethod
        fields = [
            'name_store', 'id'
        ]


class ShippingCostSerializer(serializers.ModelSerializer):
    method_name = serializers.SerializerMethodField()
    shipping_day = serializers.SerializerMethodField()

    class Meta:
        model = ShippingCost
        fields = ['ubigeo', 'price', 'id', 'method_name', 'shipping_day']

    def get_shipping_day(self, obj):
        # return FIELDNAME = models.OneToOneField()
        today_date = datetime.now().date()
        shipping_day = today_date + timedelta(obj.shipping_method.day_range)
        return shipping_day.strftime("%d/%m/%Y ")

    def get_method_name(self, obj):
        return obj.shipping_method.name_store


class InfluencerOrderSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    products = serializers.SerializerMethodField()
    methods = serializers.SerializerMethodField()

    class Meta:
        model = Influencer
        fields = ['name', 'products', 'id', 'methods']

    def get_methods(self, obj):
        ubigeo = self.context.get('ubigeo')
        shipping_methods = obj.shipping_methods.all().values_list('id', flat=True)
        shipping_cost = ShippingCost.objects.filter(ubigeo__pk=ubigeo, shipping_method__in=shipping_methods)
        return ShippingCostSerializer(shipping_cost, many=True).data

    def get_products(self, obj):
        cart_items = self.context.get('cart_items')
        cart_items_influencer = cart_items.filter(
            product__product_class__influencer__id=obj.id)
        return CartItemSerializer(cart_items_influencer, many=True).data