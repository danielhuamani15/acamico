from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps_base.core.models import (CoreTimeModel, CoreSeoSlugModel, CoreActiveModel, CorePositionModel)
from .constants import INFLUENCER, TIPO_INFLUENCER


class Influencer(CoreTimeModel, CoreActiveModel, CorePositionModel, CoreSeoSlugModel):
    name = models.CharField(_("Name"), max_length=255)
    description = models.TextField(_("Description"), blank=True)
    image = models.ImageField(_('Photo'), upload_to='influencer/%Y/%m/%d')
    banner = models.ImageField(_('Banner'), upload_to='influencer_banner/%Y/%m/%d')
    shipping_methods = models.ManyToManyField(
        'shipping.ShippingMethod',
        related_name='shipping_method_influencers', blank=True)
    type_influencer = models.CharField(
        _('Type Influencer'), max_length=120, choices=TIPO_INFLUENCER, default=INFLUENCER)

    class Meta:
        verbose_name = "Influencer"
        verbose_name_plural = "Influencers"
        permissions = (
            ('can_list_influencer', 'Influencer List'),
        )

    def __str__(self):
        return self.name
