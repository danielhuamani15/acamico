import uuid

def generate_code():
    return str(uuid.uuid4()).replace("-", "")

def validate_stock(cart):
    stock = True
    for cart_detail in cart.cart_items.all().prefetch_related('product'):
        if cart_detail.quantity > cart_detail.product.stock:
            # cart_detail = cart_detail.filter(product=cart_detail.productdetail).first()
            cart_detail.extra_data = {
                'msj': 'No contamos con el stock',
                'status': DANGER
            }
            cart_detail.save()
            stock = False
    if not stock:
        # cart = order.cart
        cart.extra_data = {
            'msj': 'Uno de los productos se quedó sin stock'
        }
        cart.save()
    return stock
