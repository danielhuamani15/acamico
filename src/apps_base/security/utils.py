from .serializers import UserSerializer

def jwt_response_payload_handler(token, user=None, request=None):
    print(list(user.get_all_permissions()))
    if user.is_superuser:
        permission = []
    else:
        permission = list(user.get_all_permissions())
    return {
        'token': token,
        'user': UserSerializer(user, context={'request': request}).data,
        'permissions': permission,
        'is_superuser': user.is_superuser
    }