from django.db.models import Sum
from .models import Order, OrderDetail, OrderCustomer, OrderShippingAddress, OrderShippingCostInfluencer
from apps_base.influencer.models import Influencer
from apps_base.customers.models import CustomerShippingAddress
from apps_base.shipping.models import ShippingCost
from apps_base.promotion.models import CouponGenerate, Coupon
from apps_base.order.constants import PROCESO
from decimal import Decimal as D, getcontext
from datetime import datetime, timedelta


class OrderGenerate(object):

    def get_influencer_total(self, order, coupon, discount):
        order_details = order.order_orderdetail.all()
        influencer_ids = order_details.values_list(
            'productdetail__product_class__influencer__id', flat=True)
        influencers = Influencer.objects.filter(id__in=influencer_ids)
        shipping_influencer = {}
        if coupon:
            influencers_coupon = coupon.influencers.all()
            total_sum_influencer = order.order_orderdetail.filter(
                productdetail__product_class__influencer__in=influencers_coupon.values_list('id', flat=True)).aggregate(Sum('total'))
            total_sum = total_sum_influencer.get('total__sum')
        for influencer in influencers:
            # total_ifn = order.order_orderdetail.filter(
            #     productdetail__product_class__influencer__id=influencer.id).aggregate(Sum('total'))
            # total_ifn = total_ifn.get('total__sum')
            total_influencer = order_details.filter(
                productdetail__product_class__influencer__id=influencer.id)
            total_influencer = total_influencer.aggregate(Sum('total')).get('total__sum', 0)
            if coupon:
                influencer_coupon = influencers_coupon.filter(id=influencer.id)
                if influencer_coupon.exists():
                    percentage = float(total_influencer / total_sum)
                    percentage = round(percentage, 2)
                    total_discount = float(discount * percentage)
                    shipping_influencer[influencer.id] = {
                        'discount_global': discount,
                        'name': influencer.name,
                        'total_sum': float(total_sum),
                        'percentage': percentage * float(100),
                        'total_discount': total_discount,
                        'total_influencer': round(float(float(total_influencer) - float(number, ndigits)), 2),
                        'total': float(total_influencer)
                    }
                else:
                    shipping_influencer[influencer.id] = {
                        'discount_global': discount,
                        'name': influencer.name,
                        'total_sum': float(0),
                        'percentage': 0,
                        'total_discount': float(0),
                        'total_influencer': float(total_influencer),
                        'total': float(total_influencer)
                    }
            else:
                shipping_influencer[influencer.id] = {
                    'discount': discount,
                    'name': influencer.name,
                    'total_sum': float(0),
                    'percentage': 0,
                    'total': float(0),
                    'total_influencer': float(total_influencer),
                    'total': float(total_influencer)
                }
        return shipping_influencer

    def get_discount_infuencer_coupon(self, coupon, discount, order):
        order_details = order.order_orderdetail.all()
        influencers = coupon.influencers.all()
        total_sum_influencer = order.order_orderdetail.filter(
            productdetail__product_class__influencer__in=influencers.values_list('id', flat=True)).aggregate(Sum('total'))
        total_sum = total_sum_influencer.get('total__sum')
        total_discount = discount
        shipping_influencer = {}
        for influencer in influencers:
            total_influencer = order_details.filter(
                productdetail__product_class__influencer__id=influencer.id)
            if total_influencer.exists():
                total_influencer = total_influencer.aggregate(Sum('total')).get('total__sum', 0)
                percentage = float(total_influencer / total_sum)
                shipping_influencer[influencer.id] = {
                    'discount': discount,
                    'name': influencer.name,
                    'total_sum': float(total_sum),
                    'percentage': percentage * float(100),
                    'total': total_discount * percentage
                }
        return shipping_influencer
        #     pass
        # shipping_influencer
        # key : {'discount': 0, 'name': 'mox', 'percentage': 20}
    def shipping_total(self, influencers, shipping_cost, order):
        # {'7': {'price': '9.00', 'method_id': 1875, 'fecha': '22/06/2018 '}}
        total_shipping = float(0)
        today_date = datetime.now().date()
        for influencer in  influencers:
            shipping_method_cost = ShippingCost.objects.get(
                id=int(shipping_cost.get(str(influencer.id)).get('method_id')))
            shipping_day = today_date + timedelta(shipping_method_cost.shipping_method.day_range)
            order_shipping_cost_influencer = OrderShippingCostInfluencer.objects.filter(
                influencer=influencer, order=order)
            if order_shipping_cost_influencer.exists():
                order_shipping_cost_influencer = order_shipping_cost_influencer.first()
                order_shipping_cost_influencer.price = shipping_method_cost.price
                order_shipping_cost_influencer.date = shipping_day
                order_shipping_cost_influencer.shipping_cost = shipping_method_cost
                order_shipping_cost_influencer.save()
            else:
                order_shipping_cost = OrderShippingCostInfluencer(
                    order=order,
                    influencer=influencer,
                    price=shipping_method_cost.price,
                    date=shipping_day,
                    shipping_cost=shipping_method_cost,
                )
                order_shipping_cost.save()
            print('shipping total')
            total_shipping = total_shipping + float(shipping_method_cost.price)
        return total_shipping

    def create(self, cart, customer, customer_data, shipping_select, shipping_cost, coupon, influencers):
        try:
            coupon_generate = Coupon.objects.get(
                prefix=coupon.strip(),  is_active=True)
            if coupon_generate.type_discount == 'PTJ':
                discount = (float(float(sub_total)*coupon_generate.discount) / 100)
            elif coupon_generate.type_discount == 'SLS':
                discount = coupon_generate.discount
        except Exception as e:
            coupon_generate = None
            discount = 0
        # shipping = ShippingCost.objects.get(ubigeo=ubigeo)
        order = Order.objects.create(
            customer=customer,
            cart=cart,
            sub_total=cart.total,
            total=float(0),
            discount=D(discount),
            shipping_price=float(0),
            type_status=PROCESO,
        )
        shipping_price_total = self.shipping_total(influencers, shipping_cost, order)
        total = float(cart.total) + shipping_price_total - float(discount)
        if coupon_generate:
            order.coupon_discount = coupon_generate
            # order.save()
        order.total = total
        self.create_details(cart, order)
        self.create_update_shipping_address(customer, shipping_select, order)
        self.create_update_customer(customer_data, order)
        shipping_influencer = self.get_influencer_total(order, coupon_generate, discount)
        order.shipping_influencer = shipping_influencer
        order.save()
        # if coupon_generate:
        #     order.shipping_influencer = self.get_discount_infuencer_coupon(coupon_generate, discount, order)
        #     order.save()
        # raise
        return order

    def update(self, cart, customer, customer_data, shipping_select, shipping_cost, coupon, influencers):
        getcontext().prec = 2
        try:
            coupon_generate = Coupon.objects.get(
                prefix=coupon.strip(), is_active=True)
            if coupon_generate.type_discount == 'PTJ':
                discount = (float(float(sub_total)*coupon_generate.discount) / 100)
            elif coupon_generate.type_discount == 'SLS':
                discount = coupon_generate.discount
        except Exception as e:
            coupon_generate = None
            discount = 0
        # shipping = ShippingCost.objects.get(ubigeo=ubigeo)
        order = Order.objects.get(cart__code=cart.code)
        print(cart, 'cart')
        shipping_price_total = self.shipping_total(influencers, shipping_cost, order)
        total = float(cart.total) + float(shipping_price_total) - float(discount)
        order.sub_total = cart.total
        order.discount = D(discount)
        order.total = total
        order.shipping_price = shipping_price_total
        order.type_status = PROCESO
        self.update_details(cart, order)
        self.create_update_shipping_address(customer, shipping_select, order)
        self.create_update_customer(customer_data, order)
        shipping_influencer = self.get_influencer_total(order, coupon_generate, discount)
        order.shipping_influencer = shipping_influencer
        if coupon_generate:
            order.coupon = coupon_generate
            # shipping_influencer = self.get_discount_infuencer_coupon(coupon_generate, discount, order)
            # order.shipping_influencer = self.get_discount_infuencer_coupon(coupon_generate, discount, order)
            # order.save()
        order.save()
        return order

    def create_update_shipping_address(self, customer, shipping_select, order):
        order_shipping_address = OrderShippingAddress.objects.filter(order=order)
        customer_shipping_address = CustomerShippingAddress.objects.get(id=int(shipping_select))
        if order_shipping_address.exists():
            order_shipping_address = order_shipping_address.first()
            order_shipping_address.first_name = customer_shipping_address.first_name
            order_shipping_address.last_name = customer_shipping_address.last_name
            order_shipping_address.address = customer_shipping_address.address
            order_shipping_address.reference = customer_shipping_address.reference
            order_shipping_address.ubigeo = customer_shipping_address.ubigeo
            order_shipping_address.document = customer_shipping_address.document
            order_shipping_address.type_document = customer_shipping_address.type_document
            order_shipping_address.phone = customer_shipping_address.phone
            order_shipping_address.shipping_address = customer_shipping_address
            order_shipping_address.save()
        else:
            order_shipping_address = OrderShippingAddress()
            order_shipping_address.order = order
            order_shipping_address.first_name = customer_shipping_address.first_name
            order_shipping_address.last_name = customer_shipping_address.last_name
            order_shipping_address.address = customer_shipping_address.address
            order_shipping_address.reference = customer_shipping_address.reference
            order_shipping_address.ubigeo = customer_shipping_address.ubigeo
            order_shipping_address.document = customer_shipping_address.document
            order_shipping_address.type_document = customer_shipping_address.type_document
            order_shipping_address.phone = customer_shipping_address.phone
            order_shipping_address.shipping_address = customer_shipping_address
            order_shipping_address.save()

    def create_update_customer(self, customer, order):
        order_customer = OrderCustomer.objects.filter(order=order)
        # {'user': {'email': 'danielhuamani15@gmail.com', 'first_name': 'Danie', 'last_name': 'Huamani'},
        # 'customer': {'type_document': 'DNI', 'phone': '959479450', 'document': '70500470', 'gender': 'M'}}
        if order_customer.exists():
            order_customer = order_customer.first()
            order_customer.first_name = customer.get('user').get('first_name')
            order_customer.last_name = customer.get('user').get('last_name')
            order_customer.email = customer.get('user').get('email')
            order_customer.phone = customer.get('customer').get('phone')
            order_customer.document = customer.get('customer').get('document')
            order_customer.type_document = customer.get('customer').get('type_document')
            order_customer.save()
        else:
            order_customer = OrderCustomer()
            order_customer.order = order
            order_customer.first_name = customer.get('user').get('first_name')
            order_customer.last_name = customer.get('user').get('last_name')
            order_customer.email = customer.get('user').get('email')
            order_customer.phone = customer.get('customer').get('phone')
            order_customer.document = customer.get('customer').get('document')
            order_customer.type_document = customer.get('customer').get('type_document')
            order_customer.save()


    def update_details(self, cart, order):
        for cart_item in cart.cart_items.all():
            try:
                order_detail = OrderDetail.objects.get(order=order, productdetail=cart_item.product)
                order_detail.quantity=cart_item.quantity
                order_detail.price=cart_item.product.price
                order_detail.sub_total=cart_item.cart_item_total
                order_detail.total=cart_item.cart_item_total
                order_detail.save()
            except Exception as e:
                order_detail = OrderDetail(
                    order=order,
                    productdetail=cart_item.product,
                    quantity=cart_item.quantity,
                    price=cart_item.product.price,
                    sub_total=cart_item.cart_item_total,
                    total=cart_item.cart_item_total,
                )
                order_detail.save()
        return None

    def create_details(self, cart, order):
        print('create')
        for cart_item in cart.cart_items.all():
            order_detail = OrderDetail(
                order=order,
                productdetail=cart_item.product,
                quantity=cart_item.quantity,
                price=cart_item.product.price,
                sub_total=cart_item.cart_item_total,
                total=cart_item.cart_item_total,
            )
            order_detail.save()
        return None