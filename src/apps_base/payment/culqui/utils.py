from .constants import ERRORES_CULQI, CULQI_VENTA_EXITOSA
from apps_base.order.utils import post_pago
import urllib
import json

def verify_charge(charge, order):
    response = {'status': 'error'}
    print(charge)
    if charge.get('object') == 'error':
        if ERRORES_CULQI.get(charge.get('type')):
            response['msg'] = ERRORES_CULQI.get(charge.get('type'))
        else:
            response['msg'] = charge.get('user_message')
    if charge.get('outcome'):
        if charge.get('outcome').get('type') == CULQI_VENTA_EXITOSA:
            response['status'] = 'ok'
            post_pago(order)
    return response,  charge.get('id')

def get_parse_cookie(cookie_value):
    parse_str = urllib.parse.unquote(cookie_value)
    return json.loads(parse_str)