from django.shortcuts import render
from django.conf import settings
from django.http import JsonResponse
from apps_base.order.models import Order
from apps_base.cart.utils import validate_stock
from apps_base.cart.models import Cart
from apps_base.order.order import OrderGenerate
from apps_base.order.constants import PAGADO
from apps_base.influencer.models import Influencer
from .constants import CULQI_VENTA_EXITOSA, ERROR_PEDIDO_PAGADO, ERROR_STOCK
from .utils import verify_charge, get_parse_cookie
from apps_web.web_order.utils import send_mail_order_success
from django.db import transaction
import culqipy
import json

culqipy.public_key = settings.ENV.get('CULQUI_PUBLIC_KEY_TEST')
culqipy.secret_key = settings.ENV.get('CULQUI_SECRET_KEY_TEST')


@transaction.atomic
def save_token(request):
    data = {'status': 'error'}
    print(request.is_ajax(), request.method)
    if request.method == 'POST':
        user = request.user
        user_customer = user.user_customer
        code_cart = request.COOKIES.get('cart', None)
        cart = Cart.objects.get(code=code_cart)
        body = request.body.decode('utf-8')
        _token = json.loads(body).get('token')
        token = _token
        status = token.get('object')
        customer_data = get_parse_cookie(request.COOKIES.get('customer', None))
        shipping_select = request.COOKIES.get('shippingSelect', None)
        shipping_cost = get_parse_cookie(request.COOKIES.get('shippingCost', None))
        order_generate = OrderGenerate()
        influencers_ids = cart.cart_items.all().values_list(
            'product__product_class__influencer', flat=True
            )
        influencers = Influencer.objects.filter(id__in=influencers_ids).distinct('id')
        coupon = None
        print(cart, user_customer, customer_data, shipping_select, shipping_cost, coupon)
        if status == 'error':
            return JsonResponse(data, status=403)
        if len(shipping_cost) != influencers.count():
            data['type_error'] = 'METODO_ENVIO'
            data['msg'] = 'No ha seleccionado los metodos de énvio'
            return JsonResponse(data, status=403)
        if Order.objects.filter(cart__code=code_cart).exists():
            order = order_generate.update(cart, user_customer, customer_data, shipping_select, shipping_cost, coupon, influencers)
        else:
            print('entro a crear')
            order = order_generate.create(cart, user_customer, customer_data, shipping_select, shipping_cost, coupon, influencers)
        # order = Order.objects.get(cart__code=code_cart)
        # order.update_total()
        print(order, 'order')
        order_customer = order.order_order_customer
        order_shipping = order.order_ordershipping
        print(order, order_customer, 'order')
        dir_charge = {
            'amount': int(order.total * 100),
            'currency_code': 'PEN',
            'description': 'Venta en Fanntop',
            'email': order_customer.email,
            'metadata': {'pedido': order.code},
            'source_id': token.get('id')
        }
        dir_charge['antifraud_details'] = {
            'first_name': order_customer.first_name[:25],
            'last_name': order_customer.last_name[:25],
            'phone_number': order_customer.phone,
            'address': order_shipping.address[:50],
            'address_city': order_shipping.ubigeo.full_ubigeo()[:50],
            'country_code': 'PE'
        }
        if order.type_status == PAGADO:
            data['msg'] = ERROR_PEDIDO_PAGADO
            return JsonResponse(data)
        if validate_stock(cart):
            charge = culqipy.Charge.create(dir_charge)
            response, id_charge = verify_charge(charge, order)
            if response.get('status') == 'ok':
                send_mail_order_success(order)
            order.extra_data = {
                'token_id': token.get('id'),
                'id_charge': id_charge
            }
            order.save()
            data.update(response)
        else:
            order.delete()
            data['type_error'] = 'STOCK'
            data['msg'] = ERROR_STOCK
    return JsonResponse(data)
