from django.conf.urls import url
from .views import departamentos, provincias, distritos, ubigeo_full_name

urlpatterns = [
    url(r'^departamentos/$', departamentos, name='api_departamentos'),
    url(r'^provincias/$', provincias, name='api_provincias'),
    url(r'^distritos/$', distritos, name='api_distritos'),
    url(r'^ubigeo-full-name/$', ubigeo_full_name, name='ubigeo_full_name'),
]
