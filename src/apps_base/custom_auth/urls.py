from django.conf.urls import url
from rest_framework import routers
from .views import UserViewSet, UserPasswordUpdate, acquire_email, GroupViewSet, PermissionAPI

router = routers.SimpleRouter()
router.register(r'users', UserViewSet)
router.register(r'group', GroupViewSet)

urlpatterns = [
    url(r"^user-password/(?P<pk>\d+)/$", UserPasswordUpdate.as_view(), name="user_password"),
    url(r"^acquire_email/$", acquire_email, name="acquire_email"),
    url(r"^permissions-codes/$", PermissionAPI.as_view(), name="permissions"),

]

urlpatterns += router.urls