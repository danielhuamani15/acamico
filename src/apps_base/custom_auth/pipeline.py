from logging import getLogger
from social_core.backends.facebook import FacebookOAuth2
from social_core.backends.google import GoogleOAuth2
from social_core.backends.linkedin import LinkedinOAuth2
from social_core.pipeline.partial import partial
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.shortcuts import render

log = getLogger('doctorcv')

# This is initially from https://github.com/python-social-auth/social-core/blob/master/social_core/pipeline/user.py
def get_username(strategy, details, backend, user=None, *args, **kwargs):
    # Get the logged in user (if any)
    logged_in_user = strategy.storage.user.get_username(user)

    # Custom: check for email being provided
    print(details, '---')
    if not details.get('email'):
        error = 'S002'  # web.constants.MSG
        return HttpResponseRedirect(reverse('web_system:home') + "?msg=" + error)

    # Custom: if user is already logged in, double check his email matches the social network email
    if logged_in_user:
        if logged_in_user.lower() != details.get('email').lower():
            error = 'S001'  # web.constants.MSG
            return HttpResponseRedirect(reverse('web_system:home') + "?msg=" + error)

    return {
        'username': details.get('email').lower(),
    }


@partial
def require_email(strategy, details, backend, user=None, is_new=False, *args, **kwargs):
    if user and user.email:
        return  # The user we're logging in already has their email attribute set
    elif is_new and not details.get('email'):
        # If we're creating a new user, and we can't find the email in the details
        # we'll attempt to request it from the data returned from our backend strategy
        user_email = strategy.request_data().get('email')
        if user_email:
            details['email'] = user_email
        else:
            # If there's no email information to be had, we need to ask the user to fill it in
            # This should redirect us to a view
            return render(strategy.request, 'system/acquire_email.html', {'backend': backend.name})
