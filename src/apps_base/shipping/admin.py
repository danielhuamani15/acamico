from django.contrib import admin
from .models import ShippingCost, ShippingMethod


admin.site.register(ShippingMethod)

class ShippingCostAdmin(admin.ModelAdmin):
    list_display = ['ubigeo', 'price']
    search_fields = ['ubigeo__desc_ubigeo_inei']

admin.site.register(ShippingCost, ShippingCostAdmin)
