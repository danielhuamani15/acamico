# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-07-13 02:55
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shipping', '0004_shippingmethod_position'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='shippingmethod',
            options={'permissions': (('can_list_shippingmethod', 'ShippingMethod List'),), 'verbose_name': 'ShippingMethod', 'verbose_name_plural': 'ShippingMethods'},
        ),
    ]
