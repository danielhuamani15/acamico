from django.conf.urls import url
from rest_framework import routers
from .views import (ShippingCostViewSet, ShippingMethodViewSet)

router = routers.SimpleRouter()
router.register(r'shipping-cost', ShippingCostViewSet)
router.register(r'shipping-method', ShippingMethodViewSet)


urlpatterns = [
]

urlpatterns += router.urls
