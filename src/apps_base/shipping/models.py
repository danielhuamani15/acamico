from django.db import models
from django.utils.translation import ugettext_lazy as _
from apps_base.core.models import CoreActiveModel, CorePositionModel

class ShippingMethod(CoreActiveModel, CorePositionModel):
    name = models.CharField(_('Name'), max_length=255)
    name_store = models.CharField(_('Name in store'), max_length=255)
    day_range = models.IntegerField(_('Day'), default=0)

    class Meta:
        verbose_name = "ShippingMethod"
        verbose_name_plural = "ShippingMethods"
        permissions = (
            ('can_list_shippingmethod', 'ShippingMethod List'),
        )

    def __str__(self):
        return self.name


class ShippingCost(models.Model):
    shipping_method = models.ForeignKey('ShippingMethod',
        related_name='shipping_methods_cost', null=True)
    ubigeo = models.ForeignKey('ubigeo.Ubigeo', related_name='ubigeo_shipping_costs')
    price = models.DecimalField(
        _("Price"), decimal_places=2, max_digits=8)

    class Meta:
        verbose_name = "ShippingPrice"
        verbose_name_plural = "ShippingPrices"
        unique_together = ('shipping_method', 'ubigeo')

    def __str__(self):
        return str(self.price)
