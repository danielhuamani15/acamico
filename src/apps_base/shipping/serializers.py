from rest_framework import serializers
from drf_queryfields import QueryFieldsMixin
from apps_base.ubigeo.models import Ubigeo
from .models import ShippingCost, ShippingMethod


class ShippingCostSerializer(serializers.ModelSerializer):
    full_ubigeo = serializers.SerializerMethodField()

    class Meta:
        model = ShippingCost
        fields = ['ubigeo', 'price', 'full_ubigeo', 'id']

    def get_full_ubigeo(self, obj):
        return obj.ubigeo.full_ubigeo()


class ShippingMethodSerializer(QueryFieldsMixin, serializers.ModelSerializer):
    lima_price = serializers.FloatField(write_only=True)
    lima_exclude_price = serializers.FloatField(write_only=True)

    class Meta:
        model = ShippingMethod
        fields = ['lima_price', 'lima_exclude_price', 'name', 'name_store', 'day_range', 'id', 'is_active', 'position']


    def create(self, validated_data):
        print(validated_data, 'va')
        lima_price = validated_data.pop('lima_price')
        lima_exclude_price = validated_data.pop('lima_exclude_price')
        instance = super().create(validated_data)
        ubigeo_lima = Ubigeo.objects.filter(cod_dep_inei=15)
        ubigeo_exclude_lima = Ubigeo.objects.exclude(cod_dep_inei=15)
        list_shipping_cost = []
        for lima in ubigeo_lima:
            list_shipping_cost.append(
                ShippingCost(
                    shipping_method=instance,
                    ubigeo=lima,
                    price=lima_price
                )
            )
        for lima in ubigeo_exclude_lima:
            list_shipping_cost.append(
                ShippingCost(
                    shipping_method=instance,
                    ubigeo=lima,
                    price=lima_exclude_price
                )
            )
        ShippingCost.objects.bulk_create(list_shipping_cost)
        return instance