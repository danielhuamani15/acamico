# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-07-13 02:34
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('category', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='category',
            options={'permissions': (('can_list_category', 'Category List'),), 'verbose_name': 'Category', 'verbose_name_plural': 'Categorys'},
        ),
    ]
